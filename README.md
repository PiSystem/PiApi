# PiApi

*Api for Pi Plugins*

 > [Usage](USAGE.md)
 
 > [Download](https://gitlab.com/PiSystem/PiApi/tags)

#### Dependencies(downloads automatic):
 - [Groovy 2.5.0](lib)
 - [Apache Ivy 2.2.0](lib)
 - [LibLoader 0.0.2](https://github.com/MPEServer/LibLoader)
 
