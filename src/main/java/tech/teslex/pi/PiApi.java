package tech.teslex.pi;

import cn.nukkit.plugin.PluginBase;
import cn.nukkit.utils.Config;
import groovy.lang.Closure;
import tech.teslex.pi.dependencies.PiDependency;
import tech.teslex.pi.utils.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class PiApi extends PluginBase {

	public static String version = "0.0.5";

	public Config config;
	public static PiApi it;

	private Scripts scriptsUtils;

	private static Dependencies dependenciesUtils;
	private static List<PiDependency> dependencies = new ArrayList<>();

	public static void registerCommands(Object o) {
		Commands.init(o);
	}

	public static void registerEvents(Object o) {
		Events.init(o);
	}

	public static List<PiDependency> getDependencies() {
		return dependencies;
	}

	public static void requireDependency(PiDependency dependency) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException {
		dependenciesUtils.initOne(dependency);
		dependencies.add(dependency);
	}

	private static List<Closure> onPluginDisableHandles = new ArrayList<>();

	public static void onPluginDisable(Closure closure) {
		onPluginDisableHandles.add(closure);
	}

	@Override
	public Config getConfig() {
		return this.config;
	}

	@Override
	public void onLoad() {
		it = this;

		saveResource(version + "/config.yml");
		saveResource(version + "/dependencies.json");

		config = new Config(getDataFolder() + "/" + version + "/config.yml");
	}

	@Override
	public void onEnable() {
		if (getConfig().getBoolean("checking_updates")) {
			try {
				getLogger().info("Checking updates..");
				checkUpdate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		dependenciesUtils = new Dependencies(this);
		loadDepends();

		scriptsUtils = new Scripts(this);
		if (getConfig().getSection("scripts").getBoolean("enabled")) {
			registerCommands(scriptsUtils);

			if (getConfig().getSection("scripts").getBoolean("autoload"))
				loadScripts();
		}
	}

	@Override
	public void onDisable() {
		onPluginDisableHandles.forEach(Closure::run);
	}

	private void loadDepends() {
		try {
			File file = new File(getServer().getPluginPath() + File.separator + "PiApi" + File.separator + version + "/dependencies.json");
			dependencies = (dependenciesUtils.loadJsonFromFile(file));
			dependenciesUtils.initAll();
		} catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private void loadScripts() {
		try {
			File scriptsDir = new File(getServer().getDataPath() + (File.separator + "scripts"));
			scriptsDir.mkdirs();

			scriptsUtils.evalAllInDir(scriptsDir);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkUpdate() throws IOException {
		String v = HTTPUtils.GET("https://gitlab.com/PiSystem/PiApi/raw/master/VERSION");

		if (!v.equals(version))
			getLogger().warning("New version found: §ev" + v);
	}
}
