package tech.teslex.pi.utils;

import cn.nukkit.command.CommandSender;
import cn.nukkit.plugin.Plugin;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.codehaus.groovy.runtime.metaclass.MissingPropertyExceptionNoStack;
import tech.teslex.pi.PiApi;
import tech.teslex.pi.annotations.PiCommand;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Scripts {

	private Plugin plugin;

	private GroovyShell groovyShell;

	public Scripts(Plugin plugin) {
		this.plugin = plugin;
		this.groovyShell = new GroovyShell(plugin.getClass().getClassLoader());
	}

	public Object evalScript(Script script) {
		plugin.getLogger().warning("Executing script: " + script.getClass().getName());

		return script.run();
	}

	public Object eval(File fileScript) throws IOException {
		if (informAboutParsiong()) plugin.getLogger().warning("Parsing script: " + fileScript.getAbsolutePath());

		Script script = groovyShell.parse(fileScript);

		return evalScript(script);
	}

	public Object eval(URI uriScript) throws IOException {
		if (informAboutParsiong()) plugin.getLogger().warning("Parsing script: " + uriScript.getPath());

		Script script = groovyShell.parse(uriScript);

		return evalScript(script);
	}

	public Object eval(String textScript) {
		if (informAboutParsiong()) plugin.getLogger().warning("Parsing script from text..");

		Script script = groovyShell.parse(textScript);

		return evalScript(script);
	}


	public void evalAllInDir(File dir) throws IOException {
		File[] scripts = dir.listFiles(file -> file.getName().endsWith(".groovy"));

		if (scripts != null) {
			for (File fileScript : scripts) {
				if (informAboutParsiong()) plugin.getLogger().warning("Parsing script: " + fileScript.getAbsolutePath());
				Script script = groovyShell.parse(fileScript);

				boolean isAutostart;

				try {
					isAutostart = (Boolean.valueOf(script.getProperty("autostart").toString()) ||
							script.getProperty("autostart") == null);
				} catch (MissingPropertyExceptionNoStack exceptionNoStack) {
					isAutostart = true;
				}

				if (isAutostart) {
					evalScript(script);
				}
			}
		}
	}

	@PiCommand(command = "piexec", description = "Execute any groovy script", usage = "/piexec <path or url>", permissions = {"pi.api.exec"})
	public void piexec(CommandSender sender, String s, String[] args) throws URISyntaxException, IOException {
		if (args.length == 0) {
			sender.sendMessage(PiApi.it.getServer().getCommandMap().getCommand("piexec").getUsage());
			return;
		}

		if (args[0].startsWith("http://") || args[0].startsWith("https://") || args[0].startsWith("file://") && plugin.getConfig().getSection("scripts").getSection("execute_by_command").getBoolean("from_url")) {
			Object response = eval(new URI(args[0]));
			if (response != null)
				sender.sendMessage("Result: " + response.toString());
		} else if (plugin.getConfig().getSection("scripts").getSection("execute_by_command").getBoolean("from_scripts_folder")) {
			Object response = eval(new File(args[0]));
			if (response != null)
				sender.sendMessage("Result: " + response.toString());
		}
	}

	private boolean informAboutParsiong() {
		return PiApi.it.getConfig().getSection("log").getBoolean("on_parsing_script");
	}
}
