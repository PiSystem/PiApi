package tech.teslex.pi.utils;

import cn.nukkit.command.Command;
import cn.nukkit.command.CommandSender;
import tech.teslex.pi.PiApi;
import tech.teslex.pi.annotations.PiCommand;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Commands {

	public static void init(Object o) {

		Arrays.stream(o.getClass().getDeclaredMethods()).filter(m -> m.isAnnotationPresent(PiCommand.class)).forEach(method -> {
			PiCommand command = method.getAnnotation(PiCommand.class);

			Command command1 = new Command(command.command(), command.description(), command.usage(), command.aliases()) {
				@Override
				public boolean execute(CommandSender commandSender, String s, String[] strings) {

					for (String permission : command.permissions())
						this.setPermission(permission);

					if (!this.testPermission(commandSender))
						return true;

					try {
						if (method.getReturnType().equals(Boolean.TYPE)) {
							return (boolean) (method.invoke(o, commandSender, s, strings));
						} else {
							method.invoke(o, commandSender, s, strings);
							return false;
						}
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					return false;
				}
			};

			if (PiApi.it.getConfig().getSection("log").getBoolean("on_register_command"))
				PiApi.it.getLogger().notice("[" + o.getClass().getName() + "] Registering command §e/" + command.command());

			PiApi.it.getServer().getCommandMap().register(command.command(), command1);
		});
	}
}
