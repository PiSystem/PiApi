package tech.teslex.pi.utils;

import cn.nukkit.event.Event;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.plugin.EventExecutor;
import cn.nukkit.utils.EventException;
import tech.teslex.pi.PiApi;
import tech.teslex.pi.annotations.PiOn;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Events {

	@EventHandler
	public static void init(Object o) {

		Arrays.stream(o.getClass().getDeclaredMethods()).filter(m -> m.isAnnotationPresent(PiOn.class) && m.getParameterTypes().length == 1).forEach(method -> {
			PiOn eventOn = method.getAnnotation(PiOn.class);

			if (PiApi.it.getConfig().getSection("log").getBoolean("on_register_event"))
				PiApi.it.getLogger().notice("[" + o.getClass().getName() + "] Registering event §e" + method.getParameterTypes()[0].getSimpleName());

			PiApi.it.getServer().getPluginManager().registerEvent(
					(Class<? extends Event>) method.getParameterTypes()[0],
					new Listener() {
					}, eventOn.priority(),
					new CustomMethodEventExecutor(method, o),
					PiApi.it,
					eventOn.ignoreCancelled()
			);
		});
	}
}

class CustomMethodEventExecutor implements EventExecutor {
	private final Method method;
	private final Object o;

	CustomMethodEventExecutor(Method method, Object o) {
		this.method = method;
		this.o = o;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Listener listener, Event event) throws EventException {
		try {
			Class<Event>[] params = (Class<Event>[]) method.getParameterTypes();
			for (Class<Event> param : params) {
				if (param.isAssignableFrom(event.getClass())) {
					method.invoke(o, event);
					break;
				}
			}
		} catch (InvocationTargetException ex) {
			throw new EventException(ex.getCause());
		} catch (ClassCastException ex) {
			// We are going to ignore ClassCastException because EntityDamageEvent can't be cast to EntityDamageByEntityEvent
		} catch (Throwable t) {
			throw new EventException(t);
		}
	}

	public Method getMethod() {
		return method;
	}
}
